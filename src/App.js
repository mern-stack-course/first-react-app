import { BrowserRouter, Route, Switch } from "react-router-dom";
import { NavBar } from "./components/NavBar";

import { Home } from "./pages/Home";
import { Services } from "./pages/Services";
import { AboutUs } from "./pages/AboutUs";
import { Login } from "./pages/Login";
import { Dashboard } from "./pages/Dashboard";

export function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route path="/services">
          <Services />
        </Route>
        <Route path="/about-us" component={AboutUs}></Route>
        <Route
          path="/login"
          render={() => {
            return <Login />;
          }}
        ></Route>
        <Route path="/dashboard">
          <Dashboard />
        </Route>
      </Switch>
    </BrowserRouter>
  );
}
