import { useState } from "react";
import { DisplayCounter } from "../components/DisplayCounter";
import { Button } from "../components/Button";
import { ProgressBar } from "../components/ProgressBar";

export const Home = () => {
  let [counter, setCounter] = useState(0); //state -> useState
  let justChangeCount = 0;
  const increment = () => {
    console.log("incrementing counter", counter);
    counter = counter + 1;
    //dom update
    setCounter(counter);
  };

  const decrement = () => {
    console.log("decrementing counter", counter);
    counter = counter - 1;
    //dom update
    setCounter(counter);
  };

  const justChange = () => {
    console.log("changing nothing", ++justChangeCount);
  };
  console.log("app rerendering");
  return (
    <div className="App">
      <h1>Learning react is easy!</h1>
      <DisplayCounter count={counter} />
      <Button label="Increment" id="abc" onClick={increment} />
      <Button
        label="Decrement"
        id="pqr"
        onClick={decrement}
        disabled={counter === 0}
      />
      <Button label="Test rendering " id="asdf" onClick={justChange} />
      <br />
      <ProgressBar></ProgressBar>
    </div>
  );
};
