import { Link } from "react-router-dom";

export const NavBar = () => {
  return (
    <header class="header">
      <nav>
        <ul class="main-menu">
          <li>
            <Link to="/">Home </Link>
          </li>
          <li>
            <Link to="/services">Services </Link>
          </li>
          <li>
            <Link to="/about-us">About us </Link>
          </li>
          <li>
            <Link to="/login">Login </Link>
          </li>
          <li>
            <Link to="/dashboard">Dashboard</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
};
