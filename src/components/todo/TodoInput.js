import { useState } from "react";

export const TodoInput = ({ onAddTodo }) => {
  const [description, setDescription] = useState("");

  const onInputChange = (e) => {
    setDescription(e.target.value);
  };
  const onAddTodoClick = (e) => {
    e.preventDefault();
    onAddTodo(description);
    setDescription("");
  };
  return (
    <form>
      <div className="element-container">
        <input
          type="text"
          name="todo_description"
          placeholder="Todo Description"
          id="todo-input"
          value={description}
          onChange={onInputChange}
        />
        <button className="btn" id="add-button" onClick={onAddTodoClick}>
          Add Todo
        </button>
      </div>
    </form>
  );
};
