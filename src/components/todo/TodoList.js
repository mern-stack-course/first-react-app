export const TodoList = ({ todos, onDeleteTodo, onEditTodo, onChangeTodo, onSaveTodo }) => {
  return (
    <table className="todo-list" id="todo-list">
      <tr>
        <th>Sr.</th>
        <th>Todo Description</th>
        <th>Operation</th>
      </tr>
      {todos.map((todo) => {
        return (
          <tr key={todo.id}>
            <td>{todo.id}</td>
            <td>
              {todo.isEdit  ?(
                <input type="text" value={todo.description} onChange={(e) => onChangeTodo(todo.id, e.target.value)} ></input>
              ) : (
                todo.description
              )}
            </td>
            <td>
              {todo.isEdit ? (
                <button className="btn save-btn" onClick={() => onSaveTodo(todo.id)}>
                  Save
                </button>
              ) : (
                <button className="btn edit-btn" onClick={() => onEditTodo(todo.id)}>
                  Edit
                </button>
              )}

              <button className="btn" onClick={() => onDeleteTodo(todo.id)}>
                Delete
              </button>
            </td>
          </tr>
        );
      })}
    </table>
  );
};
