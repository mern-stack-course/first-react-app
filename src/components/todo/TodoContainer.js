import { useEffect, useState } from "react";
import { TodoInput } from "./TodoInput";
import { TodoList } from "./TodoList";
import { ProgressBar } from "../ProgressBar";

export const TodoContainer = () => {
  const [todos, setTodos] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const todoPromise = fetch("http://localhost:5500/todos", {
      headers: {
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2NjEzNWVhOTBmZWVjMzY5MTBjMTU3MWYiLCJpYXQiOjE3MTMzMjI0ODUsImV4cCI6MTcxMzMyNjA4NX0.FSzA--6oFGCXZmrAOWTeDT4sZWmiDgBorTNpDWk5Fl4",
      },
    });
    todoPromise
      .then((response) => {
        console.log("got the success response", response);
        const dataPromise = response.json();
        dataPromise
          .then((todos) => {
            console.log("todo:", todos);
            setLoading(false);
            //transform
            const transformedTodos = todos.map((todo) => {
              return {
                ...todo,
                id: todo._id,

                isEdit: false,
              };
            });
            setTodos(transformedTodos);
          })
          .catch((err) => {
            console.log("err in parsing", err);
            setLoading(false);
          });
      })
      .catch((err) => {
        console.log("got the error responose", err);
      });
  }, []);

  const onAddTodo = (description) => {
    console.log("add todo:", description);
    //save on server
    const data = {
      description: description,
    };
    const todoPromise = fetch("http://localhost:5500/todos", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2NjEzNWVhOTBmZWVjMzY5MTBjMTU3MWYiLCJpYXQiOjE3MTMzMjI0ODUsImV4cCI6MTcxMzMyNjA4NX0.FSzA--6oFGCXZmrAOWTeDT4sZWmiDgBorTNpDWk5Fl4",
      },
      body: JSON.stringify(data),
    });

    todoPromise
      .then((res) => {
        const dataPromise = res.json();
        dataPromise.then((data) => {
          console.log("data", data);
          //react code
          todos.push({
            id: data.todoID,
            description: description,
            isEdit: false,
          });

          setTodos([...todos]);
        });
      })
      .catch((err) => {
        console.log("Error", err);
        alert("could not add todo!");
      });
  };

  const onDeleteTodo = (todoID) => {
    const deleteURL = `http://localhost:5500/todos/${todoID}`;
    const deletePromise = fetch(deleteURL, {
      method: "DELETE",
      headers: {
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2NjEzNWVhOTBmZWVjMzY5MTBjMTU3MWYiLCJpYXQiOjE3MTMzMjI0ODUsImV4cCI6MTcxMzMyNjA4NX0.FSzA--6oFGCXZmrAOWTeDT4sZWmiDgBorTNpDWk5Fl4",
      },
    });
    deletePromise
      .then((res) => {
        console.log("delete res", res);
        const newTodos = todos.filter((todo) => todo.id !== todoID);
        setTodos(newTodos);
      })
      .catch((e) => {
        console.log("delete errror", e);
        alert("Failed to delete todo");
      });
  };

  const onEditTodo = (todoID) => {
    for (let i = 0; i < todos.length; i++) {
      if (todos[i].id === todoID) {
        todos[i].isEdit = true;
      }
    }
    setTodos([...todos]);
  };

  const onSaveTodo = (todoID) => {
    //server
    let todoDescription = "";
    for (let i = 0; i < todos.length; i++) {
      if (todos[i].id === todoID) {
        todoDescription = todos[i].description;
      }
    }
    const data = {
      description: todoDescription,
    };
    const updateURL = `http://localhost:5500/todos/${todoID}`;
    const updatePromise = fetch(updateURL, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization:
          "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiI2NjEzNWVhOTBmZWVjMzY5MTBjMTU3MWYiLCJpYXQiOjE3MTMzMjI0ODUsImV4cCI6MTcxMzMyNjA4NX0.FSzA--6oFGCXZmrAOWTeDT4sZWmiDgBorTNpDWk5Fl4",
      },
      body: JSON.stringify(data),
    });
    updatePromise
      .then((res) => {
        console.log("update res", res);
        for (let i = 0; i < todos.length; i++) {
          if (todos[i].id === todoID) {
            todos[i].isEdit = false;
          }
        }
        setTodos([...todos]);
      })
      .catch((e) => {
        console.log("delete errror", e);
        alert("Failed to delete todo");
      });
  };

  const onChangeTodo = (todoID, newDescription) => {
    for (let i = 0; i < todos.length; i++) {
      if (todos[i].id === todoID) {
        todos[i].description = newDescription;
      }
    }
    setTodos([...todos]);
  };
  console.log("rendering todocontainer");
  return (
    <div className="todo-container">
      <TodoInput onAddTodo={onAddTodo}></TodoInput>
      {loading ? (
        <ProgressBar />
      ) : (
        <TodoList
          todos={todos}
          onDeleteTodo={onDeleteTodo}
          onEditTodo={onEditTodo}
          onChangeTodo={onChangeTodo}
          onSaveTodo={onSaveTodo}
        ></TodoList>
      )}
    </div>
  );
};
