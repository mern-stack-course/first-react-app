import { useEffect, useState } from "react";

export const ProgressBar = () => {
  const [progressValue, setProgressValue] = useState(0);
  console.log("setting timeout");
  useEffect(() => {
    if (progressValue < 100) {
      console.log("setting timeout");
      setTimeout(() => {
        setProgressValue(progressValue + 10);
      }, 1000);
    }
  }, [progressValue]);

  return <progress id="file" value={progressValue} max="100"></progress>;
};
