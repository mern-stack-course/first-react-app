export const Button = (props) => {
  const label = props.label;
  const id = props.id;
  const onClick = props.onClick;
  const disabled = props.disabled;
  console.log("button render");
  return (
    <button className="btn" id={id} onClick={onClick} disabled={disabled}>
      {label}
    </button>
  );
};
